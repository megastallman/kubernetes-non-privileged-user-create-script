#!/bin/bash 

#MASTER_1=""
#MASTER_2=""
#MASTER_3=""
KUBE_USER=$1
KUBE_GROUP="${KUBE_GROUP:-$KUBE_USER}"
DEFAULT_KUBECONFIG=~/.kube/config
KUBECONFIG="${KUBECONFIG:-$DEFAULT_KUBECONFIG}"
CA_LOCATION="/etc/kubernetes/ssl"

# https://docs.bitnami.com/kubernetes/how-to/configure-rbac-in-your-kubernetes-cluster/

if [ "$2" == "delete" ]
then
    echo "Deletting user ${KUBE_USER} and ALL associated resources"
    rm -f ${KUBE_USER}-kubeconfig
    ssh root@${MASTER_1} "rm ${CA_LOCATION}/${KUBE_USER}-*.pem >/dev/null"
    ssh root@${MASTER_2} "rm ${CA_LOCATION}/${KUBE_USER}-*.pem >/dev/null"
    ssh root@${MASTER_3} "rm ${CA_LOCATION}/${KUBE_USER}-*.pem >/dev/null"
    kubectl delete ns ${KUBE_USER}
    exit
fi

echo "Creating user: \"$KUBE_USER\" "

ssh root@${MASTER_1} "rm ${CA_LOCATION}/${KUBE_USER}-*.pem 2>/dev/null"
ssh root@${MASTER_1} "cd ${CA_LOCATION} && openssl genrsa -out ${KUBE_USER}-key.pem 2048"
ssh root@${MASTER_1} "cd ${CA_LOCATION} && openssl req -new -key ${KUBE_USER}-key.pem -out ${KUBE_USER}-csr.pem -subj /CN=${KUBE_USER}/O=${KUBE_GROUP}"
ssh root@${MASTER_1} "cd ${CA_LOCATION} && openssl x509 -req -in ${KUBE_USER}-csr.pem -CA kube-ca.pem -CAkey kube-ca-key.pem -CAcreateserial -out ${KUBE_USER}-crt.pem -days 10000"
scp root@${MASTER_1}:${CA_LOCATION}/${KUBE_USER}-key.pem .
scp root@${MASTER_1}:${CA_LOCATION}/${KUBE_USER}-crt.pem .
scp ${KUBE_USER}-*.pem root@${MASTER_2}:${CA_LOCATION}
scp ${KUBE_USER}-*.pem root@${MASTER_3}:${CA_LOCATION}

echo "---
apiVersion: v1
kind: Namespace
metadata:
  name: ${KUBE_USER}
spec:
  finalizers:
  - kubernetes
" | kubectl apply -f -

echo "---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  namespace: ${KUBE_USER}
  name: ${KUBE_USER}
rules:
- apiGroups: [\"\", \"extensions\", \"apps\"]
  resources: [\"*\"]
  verbs: [\"*\"]
" | kubectl apply -f -

echo "---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: ${KUBE_USER}
  namespace: ${KUBE_USER}
subjects:
- kind: User
  name: ${KUBE_USER}
  apiGroup: \"\"
roleRef:
  kind: Role
  name: ${KUBE_USER}
  apiGroup: \"\"
" | kubectl apply -f -


CLIENT_CERTIFICATE_DATA="$(cat ${KUBE_USER}-crt.pem | base64 -w 0)"
CLIENT_KEY_DATA="$(cat ${KUBE_USER}-key.pem | base64 -w 0)"

cat $KUBECONFIG | sed -E "s/client-certificate-data: (.+)/client-certificate-data: ${CLIENT_CERTIFICATE_DATA}/g; s/client-key-data: (.+)/client-key-data: ${CLIENT_KEY_DATA}/g; s/user: \"kube-admin-local\"/user: \"${KUBE_USER}\"\n    namespace: \"${KUBE_USER}\"/g; s/- name: \"kube-admin-local\"/- name: \"${KUBE_USER}\"/g" > ${KUBE_USER}-kubeconfig

rm ${KUBE_USER}-crt.pem
rm ${KUBE_USER}-key.pem
