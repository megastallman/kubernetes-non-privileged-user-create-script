# Kubernetes non-privileged user create script.
Sometimes you need to create non-privileged Kubernetes users, in order to achieve the full RBAC capabilities. That is mostly needed in restricted environments.
If you are running a 3-master Rancher Kubernetes cluster(RKE), you can use this script without any changes. Otherwise, the `CA_LOCATION` variable may vary.
Passwordless root ssh access to masters - is a requirement.

Run `MASTER_1=10.10.10.11 MASTER_2=10.10.10.12 MASTER_3=10.10.10.13 ./create-kubernetes-user.sh newusername` and grab the `newusername`'s kubeconfig file.

As a result, all 3 masters should have the same set of user keys, so that the client could authenticate against any of them.

